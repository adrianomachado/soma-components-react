export const imports = {
  'src/components/Button.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "src-components-button" */ 'src/components/Button.mdx'
    ),
  'src/components/Login.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "src-components-login" */ 'src/components/Login.mdx'
    ),
  'src/components/Form.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "src-components-form" */ 'src/components/Form.mdx'
    ),
  'src/components/NavbarPLM.mdx': () =>
    import(
      /* webpackPrefetch: true, webpackChunkName: "src-components-navbar-plm" */ 'src/components/NavbarPLM.mdx'
    ),
}
