import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import MyTheme from './MyTheme.json'
import Link from '@material-ui/core/Link';



import Grid from '@material-ui/core/Grid';
import withStyles from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
  },
  body: {backgroundColor: '#b7b8bb'},
  navbarStylish: {
    boxShadow: 'none',
    backgroundColor: '#3f51b5',
    borderBottom: '1px solid #e7e7e7',
    height: '60px',
    color: '#777',
  },
  logoSoma: {
    height: '37px',
    width: '150px',
    padding: '10px 10px',
  },
  navLink: {
    textDecoration: 'none',
    color: 'inherit'
  },
  gridLink: {
    cursor: 'pointer'
  },
  gridRight: {
    right: '0'
  }
});
const myTheme = createMuiTheme(MyTheme)

class NavbarPLM extends Component {

  constructor(props) {
    super(props);
    this.state = {
      anchorStyle: null,
      anchorCreation: null,
      anchorProduction: null,
      anchorHunt: null,
      anchorAdmin: null,
      anchorUser: null,
      usuario: JSON.parse(localStorage.getItem('user'))
    };
  }

  handleClick(e, anchorEl) {
    if(anchorEl === 'anchorStyle') this.setState({anchorStyle: e.currentTarget});
    if(anchorEl === 'anchorCreation') this.setState({anchorCreation: e.currentTarget});
    if(anchorEl === 'anchorProduction') this.setState({anchorProduction: e.currentTarget});
    if(anchorEl === 'anchorHunt') this.setState({anchorHunt: e.currentTarget});
    if(anchorEl === 'anchorAdmin') this.setState({anchorAdmin: e.currentTarget});
    if(anchorEl === 'anchorUser') this.setState({anchorUser: e.currentTarget});
  }

  handleClose = (e) => {
    this.setState({ anchorStyle: null,  anchorCreation: null, anchorProduction: null, anchorHunt: null, anchorAdmin: null, anchorUser: null});
  };

  logOut = e => {
    localStorage.removeItem('user');
  }
  
  Estilo = () => {
    const { anchorStyle, usuario } = this.state;

    if (usuario.id_estilista || usuario.admin) {
      return (
        <Grid item >
        <Grid item container onClick={e => this.handleClick(e, 'anchorStyle')} >
          <Typography variant="subtitle1" color="inherit" >
            Estilo
          </Typography>
          <ArrowDropDown />
        </Grid>
        <Menu
          anchorEl={anchorStyle}
          open={Boolean(anchorStyle)}
          onClose={e => this.handleClose(e)}
        >
          <MenuItem>
            <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/otb'} >
              OTB
            </Link>
          </MenuItem>
          <MenuItem>
            <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/quadradinhos'} >
              Quadradinhos
            </Link>
          </MenuItem>
          <MenuItem>
            <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/estampas'} >
              Estampas
            </Link>
          </MenuItem>
          <MenuItem>
            <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/painel_produto'} >
              Painel de Produto
            </Link>
          </MenuItem>
        </Menu>
      </Grid>
      )
    }
    else {
      return null
    }
  }
  render() {
    const PLM_HOMOLOG ='https://plm-homolog-dot-apt-bonbon-179602.appspot.com/#/'

    const { anchorCreation, anchorProduction, anchorHunt, anchorAdmin, anchorUser, usuario } = this.state;
    return (
      <MuiThemeProvider theme= {myTheme}>
      
      <AppBar position="static" >
        <Toolbar>
          <Grid item container spacing={40} alignItems={'center'} sm={8}>
            <Grid item>
              {/* <img
                src={'http://desenvolvimento.somagrupo.com.br/img/soma_trans.png'}
                className={styles.logoSoma}
                alt={'https://www.unesale.com/ProductImages/Large/notfound.png'}
              /> */}
            </Grid>
            {this.Estilo()}

            <Grid item>
              <Grid item container onClick={e => this.handleClick(e, 'anchorCreation')} >
              <Typography variant="subtitle1" color="inherit">
                Criação
              </Typography>
              <ArrowDropDown />
              </Grid>
              <Menu
                anchorEl={anchorCreation}
                open={Boolean(anchorCreation)}
                onClose={e => this.handleClose(e)}
              >
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/pedido_pilotagem'} >
                    Pedidos de Pilotagem
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/compra_pilotagem'} >
                    Compra de Pilotagem
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/estoque_pilotagem'} >
                    Estoque de Pilotagem
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/almox_pilotagem'} >
                    Consumo de Pilotagem
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/modelagens'} >
                    Modelagens
                  </Link>
                </MenuItem>
              </Menu>
            </Grid>

            <Grid item>
              <Grid item container onClick={e => this.handleClick(e, 'anchorProduction')} >
              <Typography variant="subtitle1" color="inherit">
                Produção
              </Typography>
              <ArrowDropDown />
              </Grid>
              <Menu
                anchorEl={anchorProduction}
                open={Boolean(anchorProduction)}
                onClose={e => this.handleClose(e)}
              >
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/simulados'} >
                    1º Gasto
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/ampliacao'} >
                    Ampliação
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/ft'} >
                    Ficha Técnica
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/alocacao'} >
                    Alocação
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/contra_amostras'} >
                    Contra Amostras
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/amostras'} >
                    Amostras Tecido
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/encaixes'} >
                    Encaixes
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/seoe'} >
                    S&OE
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/follow_up'} >
                    Controle Produção
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/ranking_fornecedores'} >
                    Ranking Fornecedores
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/compliance'} >
                    Compliance
                  </Link>
                </MenuItem>
              </Menu>
            </Grid>

            <Grid item>
              <Grid item container onClick={e => this.handleClick(e, 'anchorHunt')} >
              <Typography variant="subtitle1" color="inherit">
                Caça-Costura
              </Typography>
              <ArrowDropDown />
              </Grid>
              <Menu
                anchorEl={anchorHunt}
                open={Boolean(anchorHunt)}
                onClose={e => this.handleClose(e)}
              >
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/caca-costura'} >
                    Caça-Costura
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/admin-leilao'} >
                    Admin Caça-Costura
                  </Link>
                </MenuItem>
              </Menu>
            </Grid>


          </Grid>
          <Grid item container sm={4} justify={'flex-end'}>
            <Grid item>
              <Grid item container onClick={e => this.handleClick(e, 'anchorAdmin')} >
              <Typography variant="subtitle1" color="inherit">
                Administração
              </Typography>
              <ArrowDropDown />
              </Grid>
              <Menu
                anchorEl={anchorAdmin}
                open={Boolean(anchorAdmin)}
                onClose={e => this.handleClose(e)}
              >
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/admin/grupos'} >
                    Grupos
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/admin/opcoes'} >
                    Opções
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link underline='none' color="inherit"  href={PLM_HOMOLOG + '/admin/usuarios'} >
                    Usuários
                  </Link>
                </MenuItem>
              </Menu>
            </Grid>

            <Grid item>
                <Grid item container onClick={e => this.handleClick(e, 'anchorUser')} >
                <Typography variant="subtitle1" color="inherit">
                  {usuario.usuario.login}
                </Typography>
                <ArrowDropDown />
                </Grid>
                <Menu
                  anchorEl={anchorUser}
                  open={Boolean(anchorUser)}
                  onClose={e => this.handleClose(e)}
                >
                  <MenuItem>
                    <Link underline='none' color="inherit"  href='/'  onClick={this.logOut}>
                      Sair
                    </Link>
                  </MenuItem>
                </Menu>
              </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </MuiThemeProvider>
    )}
}



export default (NavbarPLM);
