import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import { withTheme } from '@material-ui/core/styles';
// import indigo from 'material-ui/colors/indigo';
// import pink from 'material-ui/colors/pink';
// import red from 'material-ui/colors/red'
import purple from '@material-ui/core/colors/purple';
import MyTheme from './MyTheme.json'


const myTheme = createMuiTheme(MyTheme);





class SomaButton extends Component {

  render() {

    return (
      <MuiThemeProvider theme={myTheme}>
        <Button style={{marginLeft: '8px'}}disabled={this.props.disabled} variant={this.props.variant} color={this.props.color}> Soma Button</Button>

      </MuiThemeProvider>
    )
}
}



export default (SomaButton);
