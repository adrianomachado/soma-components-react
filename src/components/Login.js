import React,  { Component, Fragment } from 'react';
// import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

// import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import CardMedia from '@material-ui/core/CardMedia';
import CircularProgress from '@material-ui/core/CircularProgress';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import RecoveryCheck from '@material-ui/icons/CheckCircleOutline';
import Error from '@material-ui/icons/Error';


// import { forgotPassword, signIn } from '../actions';

const USER_EMPTY = 'Usuário não preenchido!';
const PASS_EMPTY = 'Senha não preenchida!';
const CREDENTIALS_ERROR = 'Usuário ou senha incorretos!';


const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.grey['100'],
    height: 'auto',
  },
  grid: {
    paddingTop: theme.spacing.unit,
  },
  loadingCircle: {
    padding: theme.spacing.unit * 3,
    textAlign: 'center',
  },
  main: {
    marginTop: theme.spacing.unit * 6,
  },
  button: {
    margin: theme.spacing.unit,
  },
  pointer: {
    cursor: 'pointer',
  },
  logo: {
    width: 180,
    margin: theme.spacing.unit * 2,
  },
});

class Login extends Component {
  state = {
    username: '',
    password: '',
    showPassword: false,
    usernameError: false,
    passwordError: false,
    recoveryError: false,
    recoverySubmitted: false,
    recoveryShow: false,
  };

  resetErrors = () => {
    this.setState({
      usernameError: false,
      passwordError: false,
      recoveryError: false,
    });
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });

    this.resetErrors();
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  handleForgotPassword = () => {
    if (this.state.username) {
      this.setState(() => ({ recoverySubmitted: true }));
      return this.props.recovery(this.state.username);
    }

    this.setState(() => ({
      usernameError: USER_EMPTY,
    }));
  };

  handleGoBack = () => {
    this.resetErrors();
    window.location.reload();
  };

  handleSignIn = () => {
    const { username, password } = this.state;

    // if (username && password) {
    //   this.setState(() => ({ loginSubmitted: true }));
    //   return this.props.signIn(username, password);
    // }

    if (!username && !password) {
      return this.setState(() => ({
        usernameError: USER_EMPTY,
        passwordError: PASS_EMPTY,
      }))
    }

    if (!username) {
      return this.setState(() => ({ usernameError: USER_EMPTY }));
    }

    return this.setState(() => ({ passwordError: PASS_EMPTY }))
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!this.state.usernameError && this.props.error && this.state.recoverySubmitted) {
      this.setState(() => ({
        usernameError: this.props.error,
        recoverySubmitted: false,
      }));
    }
  }

  getRecovery() {
    const { styles } = this.props;

    return (
      <Fragment>
        <Grid item xs={1}>
          <RecoveryCheck color={'primary'} fontSize={'large'} />
        </Grid>
        <Grid item xs={9}>
          <Typography>
            Enviamos um e-mail com instruções para acessar sua conta.
          </Typography>
        </Grid>
        <Grid item xs={8}>
          <Button
            variant="contained"
            color="primary"
            className={styles.button}
            fullWidth
            onClick={this.handleGoBack}
          >
            Voltar
          </Button>
        </Grid>
      </Fragment>
    );
  }

  getErrorMessage() {
    if (this.state.loginSubmitted) {
      return (
        <Fragment>
          <Error color={'error'} fontSize={'small'} />
          <Typography variant={'body2'} gutterBottom color={'error'}>
            {CREDENTIALS_ERROR}
          </Typography>
        </Fragment>
      );
    }

    return null;
  }

  getLoginForm() {
    if (this.props.recoverySuccess) {
      return this.getRecovery();
    }

    const { loading } = this.props;
    const { usernameError, passwordError, showPassword, username, password } = this.state;

    if (loading) {
      return (
        <Grid item xs={10} className={styles.loadingCircle}>
          <CircularProgress />
        </Grid>
      );
    }

    return (
      <Fragment>
        {this.getErrorMessage()}
        <Grid item xs={10}>
          <TextField
            id={'username'}
            error={!!usernameError}
            className={styles.margin}
            variant={'outlined'}
            fullWidth
            autoFocus
            label={'Usuário'}
            value={username}
            onChange={this.handleChange('username')}
            helperText={usernameError}
          />
        </Grid>
        <Grid item xs={10}>
          <TextField
            id={'password'}
            error={!!passwordError}
            className={styles.margin}
            variant={'outlined'}
            fullWidth
            label={'Senha'}
            type={showPassword ? 'text' : 'password'}
            value={password}
            onChange={this.handleChange('password')}
            helperText={passwordError}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Mostrar senha"
                    onClick={this.handleClickShowPassword}
                    color={'secondary'}
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={8}>
          <Button
            variant="contained"
            color="primary"
            className={styles.button}
            fullWidth
            onClick={this.handleSignIn}
          >
            Login
          </Button>
        </Grid>
        <Grid item xs={12}>
          <Typography
            align="center"
            gutterBottom
            onClick={this.handleForgotPassword}
          >
            <span className={styles.pointer}>
              Esqueceu sua senha?
            </span>
          </Typography>
        </Grid>
      </Fragment>
    );
  }

  getContent() {

    return (
      <Grid item xs={12} sm={4} className={styles.grid}>
        <Paper className={styles.main}>
          <Grid container item justify={'center'}>
            {/* <CardMedia
              component={'img'}
              src={'http://desenvolvimento.somagrupo.com.br/img/soma_trans.png'}
              title="Grupo Soma"
              height={'45'}
              className={styles.logo}
            /> */}
          </Grid>

          <Grid container justify={'center'} spacing={24}>
            <Grid item xs={12}>
              <Divider variant="middle" />
            </Grid>
            {this.getLoginForm()}
          </Grid>
        </Paper>
      </Grid>
    );
  }

  render() {
    const { authToken } = this.props;

    if (authToken) {
      return (
        <Redirect to={'/home'} />
      );
    }

    return (
      <Fragment>
        <CssBaseline />
        <div className={styles.root}>
          <Grid container justify={'center'} alignItems={'center'} className={styles.grid}>
            {this.getContent()}
          </Grid>
        </div>
      </Fragment>
    );
  }
}

// const wrappedLogin = withStyles(styles)(Login);

// const mapStateToProps = (state) => ({
//   loading: state.auth.loading,
//   error: state.auth.error,
//   recoverySuccess: state.auth.recovery,
//   authToken: state.auth.token,
// });

// const mapDispatchToProps = dispatch => ({
//   recovery: (username) => dispatch(forgotPassword(username)),
//   signIn: (username, password) => dispatch(signIn(username, password)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(wrappedLogin);
export default Login;