import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyTheme from './MyTheme.json';
import { withStyles, withTheme, createMuiTheme } from '@material-ui/core/styles';



function App() {
  
  return (
    <ThemeProvider theme={createMuiTheme(MyTheme)}>...</ThemeProvider>

  );
}

export default App;
